﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;

namespace Fishtyping
{
    class Hook
    {
        //Hook of player
        Texture2D hookTexture;
        public Rectangle hookRectangle;
        Vector2 hookDefaultPosition;
        public float hookSpeed { get; set; }

        public bool hookRunning { get; set; }

        //String
        Texture2D stringTexture;
        Rectangle stringRect;

        int screenHeight;

        //sound
        SoundEffect hookDownSound;
        SoundEffectInstance hookDownSoundInstance;



        public void Initialize(Texture2D _hookTexture, Vector2 _hookPosition, float _hookSpeed, Texture2D _stringTexture, Viewport viewport, SoundEffect _hookDownSound)
        {
            hookTexture = _hookTexture;
            hookDefaultPosition = _hookPosition;
            hookRectangle = new Rectangle((int)hookDefaultPosition.X, (int)hookDefaultPosition.Y, hookTexture.Width, hookTexture.Height);


            stringTexture = _stringTexture;

            screenHeight = viewport.Height;

            stringRect = new Rectangle(hookRectangle.X + 3, hookRectangle.Y, 1, 1);
            hookRunning = false;
            hookSpeed = _hookSpeed;

            hookDownSound = _hookDownSound;
            hookDownSoundInstance = hookDownSound.CreateInstance();
        }

        public void Unload()
        {
            hookDownSoundInstance.Dispose();
        }

        public void Update(GameTime gameTime)
        {
            if (hookRunning)
            {
                hookRectangle.Y += (int)(hookSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds);
                if (hookRectangle.Y > screenHeight - hookTexture.Height)
                {
                    hookSpeed *= -2.3f;
                }
                else
                {
                    if (hookRectangle.Y < hookDefaultPosition.Y)
                    {
                        hookSpeed /= -2.3f;
                        hookRectangle.Y = (int)hookDefaultPosition.Y;
                        hookRunning = !hookRunning;
                    }
                }
                stringRect.X = hookRectangle.X + 3;
                stringRect.Height = (int)(hookRectangle.Y - hookDefaultPosition.Y);
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(hookTexture, hookRectangle, Color.White);
            spriteBatch.Draw(stringTexture, stringRect, Color.White);
        }

        public void CaughtFish()
        {
            if (hookSpeed > 0)
            {
                hookSpeed *= -2.3f;
            }
        }

        public int StringLength()
        {
            return stringRect.Height;
        }

        public void PlayHookSound()
        {
            hookDownSoundInstance.Play();
        }
    }
}
