﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Fishtyping
{
    class Animation
    {
        Texture2D spriteStrip;

        float scale;

        Rectangle sourceRect;
        Rectangle destRect;

        int elapsedTime;
        int frameTime;

        int frameCount;
        public int currentFrame;
        int startFrame;

        int nCol;

        int frameWidth;
        int frameHeight;

        public bool active { get; set; }
        bool loop;

        public bool enableAnimation { get; set; }

        public Vector2 position{ get; set; }


        public Rectangle GetRectangle()
        {
            return destRect;
        }

        public void LoadContent(Texture2D _texture)
        {
            spriteStrip = _texture;
        }

        public void SetFrameIndex(int index)
        {
            if (index >= 0 && index < frameCount)
            {
                currentFrame = index;
            }
        }

        public void Initialize(Vector2 _position, int _nCol, int _frameTime, int _frameWidth, int _frameHeight, int _frameCount, float _scale, bool _loop, bool _enableAnimation = true, int _startFrame = 0)
        {
            scale = _scale;

            elapsedTime = 0;
            frameTime = _frameTime;

            frameCount = _frameCount;
            currentFrame = _startFrame;

            nCol = _nCol;

            frameHeight = _frameHeight;
            frameWidth = _frameWidth;

            position = _position;

            active = true;
            loop = _loop;
            enableAnimation = _enableAnimation;

            startFrame = _startFrame;

        }

        public void Update(GameTime gameTime)
        {
            if (!active)
            {
                return;
            }

            // Update the elapsed time
            if (enableAnimation)
            {
                elapsedTime += (int)gameTime.ElapsedGameTime.TotalMilliseconds;

                if (elapsedTime > this.frameTime)
                {
                    currentFrame = (++currentFrame - startFrame) % frameCount + startFrame;
                    elapsedTime = 0;

                    if (!loop && currentFrame == startFrame)
                    {
                        enableAnimation = false;
                    }
                }

            }
            
            sourceRect = new Rectangle((currentFrame % nCol) * frameWidth, (currentFrame / nCol) * frameHeight, frameWidth, frameHeight);

            // Grab the correct frame in the image strip by multiplying the currentFrame index by the frame width
            destRect = new Rectangle((int)position.X - (int)(frameWidth * scale) / 2,
            (int)position.Y - (int)(frameHeight * scale) / 2,
            (int)(frameWidth * scale),
            (int)(frameHeight * scale));
        }

        public void Draw(SpriteBatch spriteBatch, SpriteEffects flip = SpriteEffects.None, float _rotation = 0f)
        {
            if (active)
            {
                spriteBatch.Draw(spriteStrip, destRect, sourceRect, Color.White, _rotation, Vector2.Zero, flip, 1f);
            }
        }

    }
}
