﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Fishtyping
{
    class Fish
    {
        Animation fishAnimation;
        Vector2 position;

        int fishType;
        float speed;
        SpriteEffects flip;

        public bool beingCaught { get; private set; }
        
        float rotation;

        public Rectangle GetRectangle()
        {
            return fishAnimation.GetRectangle();
        }

        public bool BeingCaught(float _hookSpeed, Rectangle _hookRect)
        {
            if (fishType == 4)
            {
                return false;
            }

            beingCaught = true;

            rotation = 0;
            rotation = (float)(-Math.PI / 2);

            position.X = _hookRect.X + fishAnimation.GetRectangle().Width / 2;
            position.Y = _hookRect.Y + _hookRect.Height + fishAnimation.GetRectangle().Height / 2;

            if (speed < 0)
            {
                rotation *= -1;
                position.X += fishAnimation.GetRectangle().Height / 2;
            }
            else
            {
                position.X -= fishAnimation.GetRectangle().Height / 2;
                position.Y += fishAnimation.GetRectangle().Width;
            }
            speed = _hookSpeed;

            return true;
        }

        public void Initialize(Texture2D[] _fishTextures, int _fishType, Vector2 _position)
        {
            fishAnimation = new Animation();
            fishAnimation.LoadContent(_fishTextures[_fishType - 1]);

            fishType = _fishType;

            position = _position;

            switch (_fishType)
            {
                case 1:
                    fishAnimation.Initialize(_position
                        , 5
                        , 100
                        , 180
                        , 160
                        , 9
                        , 0.5f
                        , true);
                    speed = -175;
                    break;
                case 2:
                    fishAnimation.Initialize(_position
                        , 4
                        , 100
                        , 216
                        , 151
                        , 13
                        , 0.5f
                        , true);
                    speed = -200f;
                    break;
                case 3:
                    fishAnimation.Initialize(_position
                        , 5
                        , 100
                        , 180
                        , 146
                        , 9
                        , 0.5f
                        , true);
                    speed = -255f;
                    break;
                case 4:
                    fishAnimation.Initialize(_position
                        , 6
                        , 50
                        , 154
                        , 58
                        , 20
                        , 0.7f
                        , true);
                    speed = -150f;
                    break;
                default:
                    break;
            }

            //if fish appears in the right so negative the speed;
            if (position.X == 0)
            {
                speed *= -1;
                flip = SpriteEffects.FlipHorizontally;
            }

            beingCaught = false;
            rotation = 0f;

            fishAnimation.position = position;
        }

        public void Update(GameTime gameTime)
        {
            if (beingCaught)
            {
                position.Y += speed * (float)gameTime.ElapsedGameTime.TotalSeconds;
            }
            else
            {
                position.X += speed * (float)gameTime.ElapsedGameTime.TotalSeconds;
            }
            fishAnimation.position = position;
            fishAnimation.Update(gameTime);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            fishAnimation.Draw(spriteBatch, flip, rotation);
        }

    }
}

