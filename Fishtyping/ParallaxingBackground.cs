﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Fishtyping
{
    class ParallaxingBackground
    {
        Texture2D backgroundTexture;

        Vector2[] positions;

        int speed;
        float scale;

        public int bgWidth { get; private set; }
        public int bgHeight { get; private set; }


        public void Initialize(Texture2D _texture, Viewport viewport, int speed)
        {
            backgroundTexture = _texture;
            scale = ((float)viewport.Height / 3) / _texture.Height;

            bgWidth = (int)(backgroundTexture.Width * scale);
            bgHeight = (int)(backgroundTexture.Height * scale);

            positions = new Vector2[viewport.Width / bgWidth + 2];
            for (int i = 0; i < positions.Length; i++)
            {
                positions[i] = new Vector2(i * bgWidth, 0);
            }

            this.speed = speed;
        }

        public void Update(GameTime gameTime)
        {
            for (int i = 0; i < positions.Length; i++)
            {
                positions[i].X += (int)(speed * gameTime.ElapsedGameTime.TotalSeconds);

                if (speed <= 0)
                {
                    if (positions[i].X <= -bgWidth)
                    {
                        positions[i].X = bgWidth * (positions.Length - 1);
                    }
                }
                else
                {
                    if (positions[i].X >= bgWidth * (positions.Length - 1))
                    {
                        positions[i].X = -bgWidth;
                    }
                }
            }

        }
        public void Draw(SpriteBatch spriteBatch)
        {
            for (int i = 0; i < positions.Length; i++)
            {
                Rectangle rectBg = new Rectangle((int)positions[i].X, (int)positions[i].Y, bgWidth, bgHeight);

                spriteBatch.Draw(backgroundTexture, rectBg, Color.White);
            }
        }
    }
}
